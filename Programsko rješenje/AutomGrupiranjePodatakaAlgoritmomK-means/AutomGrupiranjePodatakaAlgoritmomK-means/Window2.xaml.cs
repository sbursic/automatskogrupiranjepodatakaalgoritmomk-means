﻿/*
 * Created by SharpDevelop.
 * User: Siniša
 * Date: 07/08/2019
 * Time: 19:06
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace AutomGrupiranjePodatakaAlgoritmomK_means
{
	/// <summary>
	/// Interaction logic for Window2.xaml
	/// </summary>
	public partial class Window2 : Window
	{
		public int messageForWin1=-1;
		
		
		public Window2()
		{
			InitializeComponent();
		}
		void ok_button_Click(object sender, RoutedEventArgs e)
		{
			
			if(Functions.checkIfIntInBox(set_max_iterations.Text)==false){
				MessageBox.Show("upisi tocno");
			   }
		else	if(Int32.Parse(set_max_iterations.Text.ToString())<2)
				MessageBox.Show("Broj treba biti veći od 2");
		else	if(Int32.Parse(set_max_iterations.Text.ToString())>100)
				MessageBox.Show("Broj treba biti manji od 100");
			
			else	{
				messageForWin1= Int32.Parse(set_max_iterations.Text.ToString());
		
				Variables.maxIteration=messageForWin1;
				
				this.Hide();
			}}
		void end_button_Click(object sender, RoutedEventArgs e)
		{
			Variables.help=false;
			this.Hide();
			
		}
	}
}