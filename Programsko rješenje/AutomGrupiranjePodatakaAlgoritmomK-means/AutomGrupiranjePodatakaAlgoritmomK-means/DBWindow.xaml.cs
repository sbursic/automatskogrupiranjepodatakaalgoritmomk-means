﻿/*
 * Created by SharpDevelop.
 * User: Siniša
 * Date: 08/05/2019
 * Time: 16:51
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using OxyPlot;

namespace AutomGrupiranjePodatakaAlgoritmomK_means
{
	/// <summary>
	/// Interaction logic for DBWindow.xaml
	/// </summary>
	public partial class DBWindow : Window
	{
		public DBWindow(int minNum, int maxNum)
		{
			InitializeComponent();
			
			
			int counter=0;
			double minValue=99999999999999999;
			int minValueIndex=-1;
			this.DataContext = this;
			
			
			this.Points = new List<DataPoint>();
                            
               for(int i=minNum;i<=maxNum;i++)
               {
               	
               	Points.Add( new DataPoint(i,Variables.dbForNumberOfClusters[counter]));
               	if((Variables.dbForNumberOfClusters[counter])<minValue && Variables.dbForNumberOfClusters[counter]!=0) {
               		minValue=Variables.dbForNumberOfClusters[counter];
               		minValueIndex=i;
               	}
               	
               	
               	counter++;
               }
               MessageBox.Show("Najbolji broj grupa prema DB indeksu je "+ minValueIndex.ToString());
                   
               
               
              
            }

            public string Title { get; private set; }

            public IList<DataPoint> Points { get; private set;
		
		}
		}
	}
