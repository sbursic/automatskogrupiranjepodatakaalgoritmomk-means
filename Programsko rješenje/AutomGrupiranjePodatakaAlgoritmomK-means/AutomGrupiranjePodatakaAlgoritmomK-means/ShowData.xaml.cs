﻿/*
 * Created by SharpDevelop.
 * User: Siniša
 * Date: 4.9.2019.
 * Time: 16:23
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace AutomGrupiranjePodatakaAlgoritmomK_means
{
	/// <summary>
	/// Interaction logic for ShowData.xaml
	/// </summary>
	public partial class ShowData : Window
	{
		public ShowData(string data)
		{
			InitializeComponent();
			text1.Text= data;
		}
	}
}