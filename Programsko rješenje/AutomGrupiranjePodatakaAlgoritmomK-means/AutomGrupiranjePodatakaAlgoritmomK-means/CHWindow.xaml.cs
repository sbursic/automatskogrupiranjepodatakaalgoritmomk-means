﻿/*
 * Created by SharpDevelop.
 * User: Siniša
 * Date: 25.7.2019.
 * Time: 19:46
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using OxyPlot;

namespace AutomGrupiranjePodatakaAlgoritmomK_means
{
	/// <summary>
	/// Interaction logic for CHWindow.xaml
	/// </summary>
	public partial class CHWindow : Window
	{
		//public int  mk;
		//public int Mk;
		public CHWindow(int minNum, int maxNum)
		{
			
			InitializeComponent();
			int counter=0;
			double maxValue=0;
			int maxValueIndex=-1;
			this.DataContext = this;
			
			
			this.Points = new List<DataPoint>();
                            
               for(int i=minNum;i<=maxNum;i++)
               {
               	
               	Points.Add( new DataPoint(i,Variables.chForNumberOfClusters[counter]));
               	if((Variables.chForNumberOfClusters[counter])>maxValue) {
               		maxValue=Variables.chForNumberOfClusters[counter];
               		maxValueIndex=i;
               	}
               	
               	
               	counter++;
               }
               MessageBox.Show("Najbolji broj grupa prema CH indeksu je "+ maxValueIndex.ToString());
                   
               
               
              
            }

            public string Title { get; private set; }

            public IList<DataPoint> Points { get; private set;
		
		}
	}
}