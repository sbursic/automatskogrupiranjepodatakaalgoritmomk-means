﻿/*
 * Created by SharpDevelop.
 * User: Siniša
 * Date: 08/21/2019
 * Time: 10:42
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using OxyPlot;

namespace AutomGrupiranjePodatakaAlgoritmomK_means
{
	/// <summary>
	/// Interaction logic for FLWindow.xaml
	/// </summary>
	public partial class FLWindow : Window
	{
		public FLWindow(int minNum, int maxNum)
		{
			InitializeComponent();
			int counter=0;
			double maxValue=0;
			int maxValueIndex=-1;
			this.DataContext = this;
			
			
			this.Points = new List<DataPoint>();
                            
               for(int i=minNum;i<=maxNum;i++)
               {
               	
               	Points.Add( new DataPoint(i,Variables.flForNumberofClusters[counter]));
               	if((Variables.flForNumberofClusters[counter])>maxValue) {
               		maxValue=Variables.flForNumberofClusters[counter];
               		maxValueIndex=i;
               	}
               	
               	
               	counter++;
               }
               
                   
               
               
              
            }

            public string Title { get; private set; }

            public IList<DataPoint> Points { get; private set;
		
		}
		}
	}
