﻿/*
 * Created by SharpDevelop.
 * User: Siniša
 * Date: 8.7.2019.
 * Time: 18:59
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;



namespace AutomGrupiranjePodatakaAlgoritmomK_means
{
	/// <summary>
	/// Interaction logic for Window1.xaml
	/// </summary>
	/// <summary>
	/// Interaction logic for Window1.xaml
	/// </summary>
	public partial class Window1 : Window
	{
		public int msgFromW2=-1;
		public int numberOfData=0;
		public int dim=1;
		public int minK=0;
		public int maxK=0;
		 public Window2 w2 = new Window2();
		 
		 
		 
		

 
		
		public Window1()
		{
			InitializeComponent();
		}
		void load_data_Click(object sender, RoutedEventArgs e)
		{	
			numberOfData=0;
			dim= 1;
			
			
			var OpenFile = new Microsoft.Win32.OpenFileDialog();
			Nullable<bool> Success = OpenFile.ShowDialog();
      	    OpenFile.DefaultExt = ".txt";
            OpenFile.Filter = "Text documents (.txt)|*.txt";  //NOVO, Test za odabir datoteke
			
			 
			
			
			string[] oneNumber;
			
			int i;
			int j;
			
			if (Success == true)
				{
    				// Save document
    				string filename = OpenFile.FileName;
    			//	MessageBox.Show(filename);   //Prikaz puta do datoteke
				
			
			
			
		
			
			 
  
			
			
			string[] lines = System.IO.File.ReadAllLines(filename);	
			//string[] lines = System.IO.File.ReadAllLines(@"C:\Users\Siniša\Desktop\Sinteticki skupovi\data_k4_3.txt");
			
			
		//	string text = System.IO.File.ReadAllText(@"C:\Users\Siniša\Desktop\skup5.txt");
			
		 
			 foreach (string line in lines)
        		{
			 	//if(line.Contains(",") || line.Contains(".")){
          			  numberOfData++;
          			 // MessageBox.Show(line);
			 	//}
			 	
       			}
			
		
			
			j= lines[0].Length;
			for(i=0;i<j;i++)
			{
				
				if (lines[0][i]==' ' && lines[0][i-1]!=' ')
					dim++;
				
			}
			
			
			
			
			number_of_data_points.Text= numberOfData.ToString();
			dimension.Text= dim.ToString();
			Variables.dataPoints= new double[numberOfData,dim];
			
			try{
			
		for(i=0;i<numberOfData;i++){
					
				oneNumber= lines[i].Split(' ');
				for(j=0;j<dim;j++){
					
					Variables.dataPoints[i,j]= double.Parse(oneNumber[j].Replace(',','.'),System.Globalization.NumberStyles.AllowDecimalPoint,System.Globalization.NumberFormatInfo.InvariantInfo);
						
				}
				}
			
			}
			
			catch(Exception)
			{
				MessageBox.Show("Nije dobar tip datoteke  (.txt file)");
				numberOfData=0;
				dim=1;
				number_of_data_points.Text= numberOfData.ToString();
				dimension.Text= dim.ToString();
			}
			
		//	MessageBox.Show(Variables.dataPoints[0,0]+ " "+Variables.dataPoints[0,1]);
			}
			else {MessageBox.Show("Nije otvorena datoteka");}
			
			
		}
		
		
		
		
		public void k_means_Click(object sender, RoutedEventArgs e)
		{
			 
			
			
			
			if (Functions.checkIfIntInBox(min_K.Text)== false)
				MessageBox.Show( "min_k mora biti broj, tipa int");
			
		else  if (Functions.checkIfIntInBox(max_K.Text)== false)
   				 {
      				 MessageBox.Show("max_k mora biti broj, tipa int");
   				 }
   		
			
			
				else if( Int32.Parse(min_K.Text.ToString())<2 || Int32.Parse(max_K.Text.ToString())>50 )
					MessageBox.Show("Maksimalni ili minimalni k prešli su zadanu granicu");
				else if( Int32.Parse(min_K.Text.ToString())>= Int32.Parse(max_K.Text.ToString()))
					MessageBox.Show("Minimalni k mora biti manji od maksimalnog k");
				
				else if(numberOfData<=Int32.Parse(max_K.Text.ToString()))
					MessageBox.Show("Broj podataka mora biti veći od broja grupa (maxK) ; prije početka k-means algoritma trebaju se učitati podatci");
				else
				{
						
						minK= Int32.Parse(min_K.Text.ToString());
						maxK=Int32.Parse(max_K.Text.ToString());
						int k;
						Variables.help=true;
						
						
					
					//	w2.ShowDialog();
						
							
						
						//MessageBox.Show(Variables.maxIteration.ToString());
						int numberOfK;
						numberOfK=maxK-minK;
						Variables.chForNumberOfClusters= new double[numberOfK+1];
						Variables.dbForNumberOfClusters=new double[numberOfK+1];
						Variables.flForNumberofClusters= new double[numberOfK+1];
						int counter=0;
						for(k=minK;k<=maxK;k++)     
						{	
							if(Variables.help==true)
							w2.ShowDialog();
							Variables.centroids= new double[k,dim];   
							Variables.centroidsTemp= new double[k,dim];  
							Functions.kMeans(k,numberOfData,dim);   
							                 
							Variables.chForNumberOfClusters[counter]=Functions.CalinskiHarabaszIndex(k,numberOfData,dim);
							Variables.dbForNumberOfClusters[counter]=Functions.DavidBouldinIndex(k,numberOfData,dim);
							Variables.flForNumberofClusters[counter]=Functions.Flindex(k,numberOfData,dim);
							
							
						
						//	MessageBox.Show("ch za k= "+ k+" je jednak CH= "+ Variables.chForNumberOfClusters[counter].ToString());
							
							counter++;
							
						}
						
						
						MessageBox.Show("Broj grupa s največim CH, najmanjim DB je najprihvatljiviji izbor");
						
						
						
				
				}		
			
				
		}
		void ch_button_Click(object sender, RoutedEventArgs e)
		{
			
			
			if(minK==0 || maxK==0){
				MessageBox.Show("Prvo treba odraditi kMeans algoritam");
			}
			
			else{
				CHWindow w= new CHWindow(minK,maxK);
				
			
			
			w.Show();
			}
			
		}
		void db_button_Click(object sender, RoutedEventArgs e)
		{
			
			if(minK==0 || maxK==0){
				MessageBox.Show("Prvo treba odraditi kMeans algoritam");
			}
			
			else{
				DBWindow wd= new DBWindow(minK,maxK);
				
			
			
			wd.Show();
			}
			
		}
		void fl_button_Click(object sender, RoutedEventArgs e)
		{
			if(minK==0 || maxK==0){
				MessageBox.Show("Prvo treba odraditi kMeans algoritam");
			}
			
			else{
				FLWindow wf= new FLWindow(minK,maxK);
				
			
			
			wf.Show();
			}
		}
		
		void k_means_solution_Click(object sender, RoutedEventArgs e)
		{
				if(minK==0 || maxK==0){
				MessageBox.Show("Prvo treba odraditi kMeans algoritam");
			}
			else if(Functions.checkIfIntInBox(k_for_solution.Text)== false){
				MessageBox.Show("k mora biti tipa int");
			}
			
			else if(Int32.Parse(k_for_solution.Text.ToString())>=minK && Int32.Parse(k_for_solution.Text.ToString())<=maxK ){
				int newK= Int32.Parse(k_for_solution.Text.ToString());
				Variables.maxIteration=100;
				Functions.kMeans(newK,numberOfData,dim);
				
				string temps=null;
				
				
				for(int i=0;i<newK;i++){
					temps =temps+ "Grupa " +(i+1).ToString()+ " :";
					for(int j=0;j<numberOfData;j++){
						if(Variables.dataPointIsPartOfTheGroup[j]==i)
						{
							temps+= "\n";
							temps+= (j+1).ToString()+ "             ";
							for(int z=0;z<dim;z++){
								
								temps+= "       " + Variables.dataPoints[j,z].ToString();
							}}
						
					}
					
					
					temps=temps+"\n\n\n";
				}
			ShowData sd = new ShowData(temps);	
			sd.Show();
			
			
			}
			else {
				MessageBox.Show("k mora biti u intervalu");
			}
		}
			
			
			
		
		
		
		
		
			
			
	}
}