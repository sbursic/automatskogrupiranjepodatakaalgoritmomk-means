﻿/*
 * Created by SharpDevelop.
 * User: Siniša
 * Date: 07/08/2019
 * Time: 19:09
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.VisualBasic;


//namespace GlobalFunctions {
	
	
	static class Functions {
		
	public static bool checkIfIntInBox(string msg){
		int outParse1;
			
		if (!int.TryParse(msg, out outParse1) || String.IsNullOrWhiteSpace(msg))
   				 {
				return false;
   				 }
			else return true;
			
		}
	
	
	
	public static void kMeans(int k, int m, int dim){
		
		
		randomCentroids( k, m, dim);
		
		
		
		
		for (int i=0; i<k;i++){
			for(int j=0;j<dim;j++){
				Variables.centroidsTemp[i,j]=Variables.centroids[i,j];   // IMAMO TEMP CENTROIDE S KOJIM CEMO KASNIJE USPOREĐIVATI JESU LI SE MJESTA CENTROIDA PROMJENILA
				
			}	}
		
		bool centroidsAreChanged=false;
		
		int counterCentroidsUnchanged=0;
		int count=0;
		int iterationCounter=0;
		double minDistance;
		double minTemp;
		Variables.dataPointIsPartOfTheGroup= new int [m];
		double []summForCalculatingNewCentroidLocation= new double[dim];
		//double temp1,temp2;
		string inWhichgroup= null;
		string nowCentroids= null;
		//int t1,t2;
		
		
		
		while ( (counterCentroidsUnchanged<2) && (iterationCounter<Variables.maxIteration)){   //JEL OVAKO RADI WHILE
			
			
			
			for( int i=0;i<m;i++){    						// podatak
				minDistance= 999999999999;
				
				for(int j=0;j<k;j++){						// centroid
					
					minTemp= EuklidianDistance(dim,i,j);
					
					if (minTemp< minDistance)
					{
						
						minDistance= minTemp;
						Variables.dataPointIsPartOfTheGroup[i]= j;
						
								
					}		
					
									}
								}
			
			// OVAJ KORAK PREDSTAVLJDA PRONALAŽENJE NOVIH LOKACIJA CENTROIDIMA
			
			for(int i=0;i<k;i++){    // Centroidi
				count=0;
				for(int z=0;z<dim;z++){
					summForCalculatingNewCentroidLocation[z]=0;    
				}

				for(int j=0;j<m;j++){    //Podatci 
					
					if(Variables.dataPointIsPartOfTheGroup[j]==i){     //Ako je podatak iz promatrane grupe
						
						for(int z=0; z<dim; z++){					   //Dodajemo u ukupnu sumu 
							summForCalculatingNewCentroidLocation[z]=summForCalculatingNewCentroidLocation[z] + Variables.dataPoints[j,z];
							
						}
						count++;
					}
					
					for(int z=0;z<dim;z++){
						Variables.centroids[i,z]= (summForCalculatingNewCentroidLocation[z] /count);
					}
					
				}		
				
			}
			
			
			// OVAJ DIO DODAJE DA SE PROŠLA JEDNA ITERACIJA TE GLEDAMO JEL IMA PROMJENA KOD CENTROIDA
			centroidsAreChanged=false;
			iterationCounter++;
			for(int i =0;i<k;i++){
				for(int j=0;j<dim;j++){
				//	temp1= Math.Round(Variables.centroids[i,j]*10000000000);
				//	temp2= Math.Round(Variables.centroidsTemp[i,j]*10000000000);
				//	t1=Convert.ToInt32(temp1);
				//	t2=Convert.ToInt32(temp2);
					
					
					//USPOREDBA.......
					
					if(Variables.centroids[i,j]!=Variables.centroidsTemp[i,j]) {	//Usporedma svih elemenata centroida
						
						centroidsAreChanged=true;
						
					}
					
				}
				
			}
			if(centroidsAreChanged==false)
			{counterCentroidsUnchanged++;}
			else { counterCentroidsUnchanged=0;  }
			
			
							
			
				for (int i=0; i<k;i++){
			for(int j=0;j<dim;j++){
				Variables.centroidsTemp[i,j]=Variables.centroids[i,j];   
				
			}	}
			
		}
		
		
		
		
	//	MessageBox.Show("Sljede podatci i centroidi za k= "+ k+"  " );
			
		for(int i=0;i<m;i++){    //ZA PROVJERU
								//MessageBox.Show("pripadanje grupi :"+Variables.dataPointIsPartOfTheGroup[i].ToString()+ " ");
								inWhichgroup+=Variables.dataPointIsPartOfTheGroup[i].ToString()+ " ";
								            
							}
		//	MessageBox.Show(inWhichgroup);
			inWhichgroup=null;
						
							for(int j=0;j<k;j++){			//ZA PROVJERU
								//MessageBox.Show(Variables.centroids[j,0].ToString()+"  "+Variables.centroids[j,1].ToString());
								nowCentroids+= Variables.centroids[j,0].ToString()+"  "+Variables.centroids[j,1].ToString()+"  " + "\n";
							}
		//	MessageBox.Show(nowCentroids);
			nowCentroids=null;
	
	
	
	
	}
	
	public  static void randomCentroids(int k, int m, int dim){
		
	
		
	Random rnd = new Random();
	int random;
	
	List<int> used= new List<int>();
	
	for(int i=0;i<k;i++){
		
		do{
			random  = rnd.Next(0, m);
			
			
		}while((used.Contains(random)));
		
		
	for(int j=0;j<dim; j++){
			Variables.centroids[i,j]=Variables.dataPoints[random,j]; 
	}
	used.Add(random);
	
		
	}
	
	used.Clear();
	}
	
	public static double EuklidianDistance( int dim,int rowData, int rowCentroid){
		
		double distance=-1;
		double summ=0;
		
		
		for(int i=0; i<dim;i++)
		{
			summ = summ + Math.Pow(Variables.dataPoints[rowData,i]-Variables.centroids[rowCentroid,i],2);
		}
		
		distance = Math.Sqrt(summ);
		
		return distance;
		
		
	}
	
	
	public static double CalinskiHarabaszIndex(int k,int m, int dim){
		
		double fls=0;
		double g=0;
		double gtemp=0;
		double ch=0;
		
		int	[]elementsInGroup;
		elementsInGroup= new int[k];
		double []mainCenroid;
		mainCenroid=new double[dim];
		int centroidIndex;
		
		
		for(int i=0;i<m;i++){     
			centroidIndex=Variables.dataPointIsPartOfTheGroup[i];
			fls = fls+ dissipation(i, centroidIndex, dim);
		}
	//	MessageBox.Show("Fls= "+ fls);   // ZA PROVJERU
		
		
		string mainCentroidString=null;
		
		//Centroid citavog skupa podataka
		for(int i=0;i<dim;i++){
			mainCenroid[i]=0;
			for(int j=0; j<m;j++){
				mainCenroid[i]+=Variables.dataPoints[j,i];
				
				
			}
			mainCenroid[i]= mainCenroid[i]/m;
			mainCentroidString+= mainCenroid[i].ToString()+ "   ";   
			
		}
		
	//	MessageBox.Show("Glavni centroid je: "+ mainCentroidString);  //OVAJ DIO JE TOČAN
		
		
		
		//G
		
		//for(int i=0;i<k;i++){  
		//	elementsInGroup[i]=0;
		//	for(int j=0;j<m;j++){
		//		if(Variables.dataPointIsPartOfTheGroup[j]==i){
		//			elementsInGroup[i]++;            //DOBIJAMO BROJ ELEMENATA U SVAKOJ GRUPI
		//		}
		//	}
		//}
		
		elementsInGroup=Functions.NumberOfDataInAGroup(k,m);
		// PROVJERA JEL RADI FUNKCIJA
		//for(int i=0; i<k;i++){
		//	MessageBox.Show("Broj elementiju"+ elementsInGroup[i].ToString());
		//}
		
		
		for(int i=0;i<k;i++){
			gtemp=0;
			for(int j=0;j<dim;j++){
				gtemp= gtemp+(Math.Pow((Variables.centroids[i,j]-mainCenroid[j]),2));
				
			}
			g= g+(gtemp*elementsInGroup[i]);    	
			
		}
	
//	MessageBox.Show("G= "+ g);  //ZA PROVJERU
		
			
		
		
		
		
		//CH
		
		ch= (g/(k-1))/(fls/(m-k));    
			
			return ch;
		
	}
	
	
	public static double DavidBouldinIndex (int k, int m, int dim){
		
		double []standardDEviation= new double[k];
		int []elementsInAGroup= new int[k];
		double findMax,max,temp,db=0;;
		
		elementsInAGroup=Functions.NumberOfDataInAGroup(k,m);
		
		//Standatdna devijacija
		
		for(int i=0;i<k;i++){
			for(int j=0;j<m;j++){
				if(Variables.dataPointIsPartOfTheGroup[j]==i){
					 
					for(int l=0;l<dim;l++){
						standardDEviation[i]=standardDEviation[i]+ Math.Pow((Variables.centroids[i,l] - Variables.dataPoints[j,l]),2);
					}
				}
				
				
			}
			standardDEviation[i]= Math.Sqrt(standardDEviation[i]/elementsInAGroup[i]); 

			//MessageBox.Show("Standardn devijacija za grupu"+i.ToString()+"   "+ standardDEviation[i].ToString());			
			
			}
		
		
		
		
		
		//RACUNANJE DB INDEKSA      
			
			for(int i=0; i<k;i++){
			//db=0;
			findMax=0;
			max=0;
			temp=0;
			for(int j=0;j<k;j++){
				if(j!=i){
					findMax= (standardDEviation[i]+standardDEviation[j]);
						for(int l=0;l<dim;l++){
						temp=temp+ Math.Sqrt(Math.Pow((Variables.centroids[i,l])-(Variables.centroids[j,l]),2));
					}
					findMax=findMax/temp;
					if(findMax>max)
					{max=findMax;}  	
					
				}					
			}
		
				//	MessageBox.Show("maksimum za "+i.ToString()+"     "+max.ToString());
				db=db+max;
				//	MessageBox.Show(db.ToString());
				
			}
		db=db/k;
		
		return db;		
	}
	
	
	
	
	
	public static double dissipation(int rowData, int rowCentroid, int dim){
		
		double value=0;
		
		for(int i=0;i<dim;i++){
			value = value + Math.Pow((Variables.centroids[rowCentroid,i]-Variables.dataPoints[rowData,i]),2);
			
			
		}
		return value;
		
	}
	
	
	
	public static int[] NumberOfDataInAGroup(int k,int m ){
		
		
		int []dataInAGroup= new int[k];
		for(int i=0;i<k;i++){  
			dataInAGroup[i]=0;
			for(int j=0;j<m;j++){
				if(Variables.dataPointIsPartOfTheGroup[j]==i){
					dataInAGroup[i]++;            //DOBIJAMO BROJ ELEMENATA U SVAKOJ GRUPI
				}
			}
		}
		
		return dataInAGroup;
			
		
		
	}
	
	
	public static double Flindex (int k,int m , int dim){
		double fls=0;
		
		
		
		int	[]elementsInGroup;
		elementsInGroup= new int[k];
		double []mainCenroid;
		mainCenroid=new double[dim];
		int centroidIndex;
		
		
		for(int i=0;i<m;i++){     
			centroidIndex=Variables.dataPointIsPartOfTheGroup[i];
			fls = fls+ dissipation(i, centroidIndex, dim);
		}
		
		
		return fls;
		
	}
	
	
}
	
	
	
	
static class Variables {
	
	public static double [,]dataPoints;    			 // Tu će se spremasti podatci učitani iz datoteke
	public static double [,]centroids;    			 // Tu će biti spremljeni centroidi
	public static int []dataPointIsPartOfTheGroup;   //Tu će svaki podatak imati svoj broj koji pokazuje kojem centroidu pripada
	public static double [,] centroidsTemp;				 //Ovo nisam siguran jel mi treba
	public static int maxIteration=100;
	public static bool help=true;
	
	
	public static double [] chForNumberOfClusters;
	public static double [] dbForNumberOfClusters;
	public static double [] flForNumberofClusters;
	
	
}
	
	
	
	// TO DO LIST
	
	
	// --CH INDEKS AKO JE BROJ K=1 JE NaN ? --
	
	
	
	//-- Biranje iteracija drugi put kad kmeans radimo (Hide umjesto close) ?
	
	
	//--- Mozda je bolje i=1 do k umjesto i=0 -----
	
	
	//---- PROVJERITI JEL DOBRO DB INDEKS RADI NA 1D PRIMJERU    SUTRA.....Provjereno i nađena greška pri traženju max
	
	
	
	
	
	
	
//}